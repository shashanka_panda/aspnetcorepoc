namespace Sainsburys.Server.Models
{
 public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Quantity { get; set; }
        public double CpstPerUnit { get; set; }
        public string Description { get; set; }
        public string ImageUrl {get; set; }
    }

}