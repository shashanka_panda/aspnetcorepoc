namespace Sainsburys.Server.Models
{
 public class Sale
    {
        public long Id { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
        public string Location { get; set; }
    }
}