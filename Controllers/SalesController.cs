using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Sainsburys.Server.Models;
using Microsoft.AspNetCore.Cors;

namespace Sainsburys.Server.Controllers
{
    [Route("api/[controller]")]
     [EnableCors("AllowAll")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly DataContext _context;

        public SalesController(DataContext context)
        {
            _context = context;

            if (_context.Sales.Count() == 0)
            {
                // Create a new Product if collection is empty,
                // which means you can't delete all Products.
                _context.Sales.Add(new Sale {Location="UK",ProductId= "1"    });
                _context.SaveChanges();
            }
        }
        [HttpGet]
        public ActionResult<List<Sale>> GetAll()
        {
            return _context.Sales.ToList();
        }

        [HttpGet("{id}", Name = "GetSales")]
        public ActionResult<Sale> GetById(long id)
        {
            var item = _context.Sales.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }


    }
}