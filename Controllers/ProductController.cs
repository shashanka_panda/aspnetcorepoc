using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Sainsburys.Server.Models;
using Microsoft.AspNetCore.Cors;

namespace Sainsburys.Server.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAll")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly DataContext _context;

        public ProductController(DataContext context)
        {
            _context = context;

            if (_context.Products.Count() == 0)
            {
                // Create a new Product if collection is empty,
                // which means you can't delete all Products.
                _context.Products.Add(new Product { Name = "Product1" });
                _context.SaveChanges();
            }
        }
        [HttpGet]
        public ActionResult<List<Product>> GetAll()
        {
            return _context.Products.ToList();
        }

        [HttpGet("{id}", Name = "GetProduct")]
        public ActionResult<Product> GetById(long id)
        {
            var item = _context.Products.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }


    }
}